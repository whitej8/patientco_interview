var Dispatcher = require('flux').Dispatcher;
var AppDispatcher = new Dispatcher();
var GraphStore = require('../stores/GraphStore');

AppDispatcher.register(function (action) {
  switch(action.actionType) {
    // case 'ADD_NEW_ITEM':
    //   GraphStore.addNewItemHandler(action.text);
    //   GraphStore.emitChange();
    //   break;
    default:
      // no op
  }
})

module.exports = AppDispatcher;
