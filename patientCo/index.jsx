var React = require('react');
var ReactDOM = require('react-dom');
var MyGraphController = require('./components/MyGraphController');

ReactDOM.render(
  <MyGraphController/>,
  document.querySelector('#myGraph')
);
