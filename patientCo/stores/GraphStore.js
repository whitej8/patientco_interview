var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var GraphStore = assign({}, EventEmitter.prototype, {
  data: {},

  init: function () {
    //pull form json src but hardcode for example
      this.data = {
        "context": "Patient Payments",
        "center": "2017 YTD",
        "slices": [{
            "name": "Front Office",
            "value": "24.3%",
            "detailName": "frontOfficeRateTrending"
          },
          {
            "name": "Back Office",
            "value": "47.8%",
            "detailName": null
          },
          {
            "name": "Self-Service",
            "value": "27.9%",
            "detailName": null
          }
        ],
        "highlights": [{
            "name": "$ Collected",
            "value": "$7,431,599",
            "detailName": "patientCollectedTrending"
          },
          {
            "name": "Avg Trans Amt",
            "value": "$117",
            "detailName": null
          },
          {
            "name": "Avg A/R Days",
            "value": "23",
            "detailName": null
          }
        ]
      };      
  },

  getAll: function () {
    return this.data;
  },

  emitChange: function () {
    this.emit('change');
  },

  addChangeListener: function(callback) {
    this.on('change', callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener('change', callback);
  }
});

module.exports = GraphStore;