var React = require('react');
var GraphStore = require('../stores/GraphStore');
var MyGraph = require('./MyGraph');

var MyGraphController = React.createClass({
  getInitialState: function () {

    GraphStore.init();
    return {
      items: GraphStore.getAll()
    };
  },

  componentDidMount: function() {
    GraphStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    GraphStore.removeChangeListener(this._onChange);
  },

  _onChange: function () {
    this.setState({
      items: GraphStore.getAll()
    });
  },

  updateEntry: function (event) {
    GraphStore.updateEntry();
  },

  render: function() {
    return <MyGraph
      data={this.state.items}
    />;
  }

});

module.exports = MyGraphController;
