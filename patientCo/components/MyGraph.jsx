var React = require('react');

//var DoughnutChart = require("react-chartjs").Doughnut;

var MyGraph = function(props) {
  var data = props.data;

  var highlightHtml = data.highlights.map(function (hl, i) {
    return <div key={i} className="highlight col" style={{marginLeft: (i*20+20) + 'px'}}>
      <div className="value">{hl.value}</div>
      <div className="name">{hl.name}</div>
    </div>;
  });

  var sliceHtml = data.slices.map(function (s, i) {
    return <div key={i} className="slice col1">
      <div className="value">{s.value}</div>
      <div className="name">{s.name}</div>
    </div>;
  });  

  return <div className="card flex-grid">
    <div className="col1">
      <div className="context">{data.context}</div>
      <div className="highlightsWrapper">{highlightHtml}</div>
    </div>
    <div className="sliceWrapper col2 flex-grid">{sliceHtml}</div>
  </div>;
}

module.exports = MyGraph;


        // "highlights": [{
        //     "name": "$ Collected",
        //     "value": "$7,431,599",
        //     "detailName": "patientCollectedTrending"
        //   },